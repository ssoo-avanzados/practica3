#include <stdio.h>
#include <stdlib.h>
#include "leercar_R.h"
#include "contar_M.h"

int main(int argc, char *argv[]) {
    int fd, resultado=0; 

    if(argc != 4) {
        printf("Error, número de parámetros incorrecto\n");
        return -1;
    }
    fd=open(argv[2], O_RDONLY);
    if(fd < 0) {
        printf("Error al abrir el archivo\n");
        return -1;
    }

    if(argv[1][0] == 'R') {
        int i =1;
        while(LeerCaracter(fd, i)!=EOF) {
            if(LeerCaracter(fd, i)==argv[3][0]) {
                resultado++;
            }
            i++;
        }
        
        printf("La letra %s aparece %d veces\n", argv[3], resultado);
    }
    else if(argv[1][0] == 'M') {
        resultado = ContarCaracteres(fd, argv[3][0]);
        printf("La letra %s aparece %d veces\n", argv[3], resultado);
    }
    else {
        printf("Error, opción no válida\n");
        return -1;
    }
    close(fd);
    return 0;
}