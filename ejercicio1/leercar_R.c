#include "leercar_R.h"

char LeerCaracter (int fd, int posicion){

   char caracter[1];
   if (lseek(fd, posicion, SEEK_SET)<0) return EOF;   //Control de errores  
   if (read(fd, caracter, 1)==0) return EOF;          //Control de errores
   return caracter[0]; 
   
}