#include "contar_M.h"

int ContarCaracteres(int fd, char caracter) {
    int cont = 0;
    int length = lseek(fd, 0, SEEK_END);
    lseek(fd, 0, SEEK_SET);
    char* fichero =  mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0);
    
    for(int i=0; i<length; i++) {
        if(fichero[i] == caracter) {
            cont++;
        }
    }
    
    return cont;
}