/*
    prueba.c
*/
#include <stdio.h>
#include <stdlib.h>
#include "parser.h"
#include "internas.h"
#include <string.h>

void mostrar_orden (orden * O);

int main ()
{
    orden O;
    orden* pOrden = &O;
    int r;

    do              // Leer órdenes y mostrarlas
    {
        inicializar_orden (&O);

        printf("\x1b[31m");
        printf ("µShell@MicroShell: ");
        printf("\x1b[0m");
        r = leer_orden (&O, stdin);

        if (r < 0)
            fprintf (stderr, "\nError %d: %s\n",
                             -r, mensajes_err[-r]);
        else {
            if(strcmp(pOrden->argv[0],"pwd")==0) {
                    mipwd();
            }
            else if(strcmp(pOrden->argv[0],"ls")==0) {
                    mils(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"mkdir")==0) {
                    mimkdir(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"rmdir")==0) {
                    mirmdir(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"cd")==0) {
                    micd(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"cat")==0) {
                    micat(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"cp")==0) {
                    micp(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"rm")==0) {
                    mirm(pOrden);
            }
            else if(strcmp(pOrden->argv[0],"exit")==0) {
                    miexit();
            }
            else {
                    printf("Error orden no válida\n");
            }
        }
        liberar_orden (&O);
    }
    while (r==0);   // Repetir hasta error o EOF

    return 0;
}

void mostrar_orden (orden * O)
{
    int i;

    printf ("\tOrden cruda: \"%s\"\n", O->orden_cruda);
    printf ("\tNúmero de argumentos: %d\n", O->argc);

    for (i=0; i<=O->argc; i++)
        if (O->argv[i] != NULL)
            printf ("\t\targv[%d]: \"%s\"\n", i, O->argv[i]);
        else
            printf ("\t\targv[%d]: NULL\n", i);

    if (O->entrada)
        printf ("\tEntrada: \"%s\"\n", O->entrada);

    if (O->salida)
        printf ("\tSalida: \"%s\"\n", O->salida);

    if (O->salida_err)
        printf ("\tSalida de err.: \"%s\"\n", O->salida_err);

    printf ("\tEjecutar en segundo plano: %s\n",
            O->segundo_plano ? "Sí" : "No");
}


