#include "internas.h"

#include <string.h>

void mipwd() {
    char dir[256];
    if(getcwd(dir, sizeof(dir))) {
        printf("%s\n",dir);
    }
    else {
        printf("Error en pwd\n");
    }
}

void mimkdir(orden* O) {
    if(mkdir(O->argv[1],0755)<0) {
            printf("Error al crear el directorio\n");
    }
    else {
        printf("Directorio creado con éxito\n");
    }
}

void mirmdir(orden* O) {  
    if(rmdir(O->argv[1])<0) {
        printf("Error al eliminar el directorio\n");
    }
    else {
        printf("Directorio eliminado con éxito\n");
    }
}

void micd(orden* O) {
    if(O->argv[1] == NULL) {
        if(chdir(getenv("HOME"))<0) {
            printf("Error al cambiar de directorio\n");
        }
    }
    else {
        if(chdir(O->argv[1])<0) {
            printf("Error al cambiar de directorio\n");
        }
    }
}

void micat(orden* O) {
    int length;
    struct stat s;
    char * archivo = O->argv[1];
    int fd = open(archivo, O_RDONLY);

    if(fd<0) {
        printf("Error al abrir el archivo\n");
    }
    else {
        fstat(fd, &s);
        length = s.st_size;
        char* fichero =  mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0);
        printf("%s\n",fichero);
    }
}

void micp(orden* O) {

    char buffer[1024];
    int files[2];
    ssize_t count;

    if (O->argc < 3) {        //Si hay menos de 3 argumentos
        printf("Error, parámetros incorrectos\n");
    }
    else {
        files[0] = open(O->argv[1], O_RDONLY);  //se mete el archivo en files
        if (files[0] == -1) {                  //Si pasase algo raro en el primer archivo
            printf("Error al copiar el archivo\n");
        }
        else {
            files[1] = open(O->argv[2], O_WRONLY | O_CREAT | S_IRUSR | S_IWUSR);
            if (files[1] == -1)             //Si pasase algo raro en el segundo archivo
            {
                printf("Error al copiar el archivo\n");
            }
            else {
                while ((count = read(files[0], buffer, sizeof(buffer))) != 0) {
                    write(files[1], buffer, count);
                }
                close(files[0]);
                close(files[1]);
            }
        }
    }
}

void mirm(orden* O) {
    if(unlink(O->argv[1])<0) {
        printf("Error al eliminar el archivo\n");
    }
    else {
        printf("Archivo eliminado con éxito\n"); 
    }
}

void miexit() {
    exit(0);
}

void mils(orden* O){

    char dir[256];
    DIR *mydir;
    struct dirent *myfile;
    struct stat mystat;

    if((O->argv[2]!=NULL) ){                        //Si el 2 argumento no es null

        if(strcmp(O->argv[2],"-l")==0){             //Si tiene la opcion -l en 2 posicion, tiene directorio metido

            mydir = opendir(O->argv[1]);

            if(mydir) {
                while((myfile = readdir(mydir)) != NULL) {
                stat(myfile->d_name, &mystat); 

                time_t ltime;
                ltime =  mystat.st_mtime;

                printf("\x1b[93m");
                    printf( (S_ISDIR(mystat.st_mode)) ? "d" : "-" );    //Permisos
                    printf( (mystat.st_mode & S_IRUSR) ? "r" : "-" );
                    printf( (mystat.st_mode & S_IWUSR) ? "w" : "-" );
                    printf( (mystat.st_mode & S_IXUSR) ? "x" : "-" );
                    printf( (mystat.st_mode & S_IRGRP) ? "r" : "-" );
                    printf( (mystat.st_mode & S_IWGRP) ? "w" : "-" );
                    printf( (mystat.st_mode & S_IXGRP) ? "x" : "-" );
                    printf( (mystat.st_mode & S_IROTH) ? "r" : "-" );
                    printf( (mystat.st_mode & S_IWOTH) ? "w" : "-" );
                    printf( (mystat.st_mode & S_IXOTH) ? "x" : "-" );
                    printf("\t ");
                    printf("\x1b[0m");

                    printf("\x1b[94m");
                    printf(" %s \t ", myfile->d_name);        //Nombre
                    printf("\x1b[0m");

                    if(!(strcmp(myfile->d_name,"."))||!(strcmp(myfile->d_name,".."))){
                        printf("\t %ld \t",mystat.st_size);          //Tamaño
                    }
                    else{
                        printf("%ld \t",mystat.st_size);          //Tamaño
                    }

                    printf("%d \t",mystat.st_uid);            //User is
                    printf("%d \t",mystat.st_gid);            //Group id    

                    printf("\x1b[32m");

                    if(S_ISLNK(mystat.st_mode)){              //Tipo de archivo
                        printf("Link \t");
                    }
                    if(S_ISREG(mystat.st_mode)){
                        printf("Reg \t");
                    }
                    if(S_ISDIR(mystat.st_mode)){
                        printf("Dir \t");
                    }
                    if(S_ISFIFO(mystat.st_mode)){
                        printf("Fifo \t");
                    }
                    if(S_ISSOCK(mystat.st_mode)){
                        printf("Socket \t");
                    }
                    if(S_ISBLK(mystat.st_mode)){
                        printf("Block \t");
                    }
                    if(S_ISCHR(mystat.st_mode)){
                        printf("Char \t");
                    }
                    printf("\x1b[0m");

                printf("\x1b[35m");
                printf("%s \t \n",ctime(&ltime));         //Ultima mod   
                printf("\x1b[0m");      
            }
            closedir(mydir);
            }
            else {
                printf("Error, el directorio no existe\n");
            }
        }
    }

    else if(O->argc==1){                               //Si solo está el ls

            mydir = opendir(getcwd(dir, sizeof(dir)));

            if(mydir) {
                while((myfile = readdir(mydir)) != NULL) {
                stat(myfile->d_name, &mystat);    
                printf(" %s \n", myfile->d_name);    //Nombre
            }
            closedir(mydir);
            }
            else {
                printf("Error, el directorio no existe\n");
            }
    }

    else{

        if(strcmp(O->argv[1],"-l")==0){                             //Si tiene la opcion -l en 2 posicion FUNKA

            mydir = opendir(getcwd(dir, sizeof(dir)));

            if(mydir) {
                while((myfile = readdir(mydir)) != NULL) {
                stat(myfile->d_name, &mystat); 

                time_t ltime;
                ltime =  mystat.st_mtime;

                printf("\x1b[93m");
                printf( (S_ISDIR(mystat.st_mode)) ? "d" : "-" );    //Permisos
                printf( (mystat.st_mode & S_IRUSR) ? "r" : "-" );
                printf( (mystat.st_mode & S_IWUSR) ? "w" : "-" );
                printf( (mystat.st_mode & S_IXUSR) ? "x" : "-" );
                printf( (mystat.st_mode & S_IRGRP) ? "r" : "-" );
                printf( (mystat.st_mode & S_IWGRP) ? "w" : "-" );
                printf( (mystat.st_mode & S_IXGRP) ? "x" : "-" );
                printf( (mystat.st_mode & S_IROTH) ? "r" : "-" );
                printf( (mystat.st_mode & S_IWOTH) ? "w" : "-" );
                printf( (mystat.st_mode & S_IXOTH) ? "x" : "-" );
                printf("\t ");
                printf("\x1b[0m");

                printf("\x1b[94m");
                printf(" %s \t ", myfile->d_name);        //Nombre
                printf("\x1b[0m");

                if(!(strcmp(myfile->d_name,"."))||!(strcmp(myfile->d_name,".."))){
                    printf("\t %ld \t",mystat.st_size);          //Tamaño
                }
                else{
                    printf("%ld \t",mystat.st_size);          //Tamaño
                }

                printf("%d \t",mystat.st_uid);            //User is
                printf("%d \t",mystat.st_gid);            //Group id

                printf("\x1b[32m");

                if(S_ISLNK(mystat.st_mode)){              //Tipo de archivo
                    printf("Link \t");
                }
                if(S_ISREG(mystat.st_mode)){
                    printf("Reg \t");
                }
                if(S_ISDIR(mystat.st_mode)){
                    printf("Dir \t");
                }
                if(S_ISFIFO(mystat.st_mode)){
                    printf("Fifo \t");
                }
                if(S_ISSOCK(mystat.st_mode)){
                    printf("Socket \t");
                }
                if(S_ISBLK(mystat.st_mode)){
                    printf("Block \t");
                }
                if(S_ISCHR(mystat.st_mode)){
                    printf("Char \t");
                }
                printf("\x1b[0m");

                printf("\x1b[35m");
                printf("%s \t \n",ctime(&ltime));         //Ultima mod   
                printf("\x1b[0m");
            }
            closedir(mydir);
            }
            else {
                printf("Error al recuperar la información del directorio\n");
            }
        }

        else if(O->argc==2){                                       //ls + ruta   FUNKA

            mydir = opendir(O->argv[1]);

            if(mydir) {
                while((myfile = readdir(mydir)) != NULL)
            {
                stat(myfile->d_name, &mystat);    
                printf(" %s \n", myfile->d_name);    //Nombre
            }
            closedir(mydir);
            }
            else {
                printf("Error al recuperar la información del directorio\n");
            }

        }
        else {
            printf("Error, parámetros no válidos\n");
        }

    }

 

}
